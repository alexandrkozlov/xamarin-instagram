using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using InstagramDemo.Core;

namespace InstagramDemo.Android
{
	[Activity(Label = "Photos")]			
	public class PhotosActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.PhotosActivity);

			this.Title = this.Intent.Extras.GetString("name");
			Instagram.GetMediaForLocation(this.Intent.Extras.GetInt("id"), media => {
				GridView gridView = (GridView)this.FindViewById(Resource.Id.gridView1);
				gridView.Adapter = new InstagramImageAdapter(this, media);
			});
		}
	}
}

