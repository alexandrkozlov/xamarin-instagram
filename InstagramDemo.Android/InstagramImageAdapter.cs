using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using InstagramDemo.Core;
using Android.Graphics.Drawables;
using Java.Net;
using Android.Net;
using Android.Graphics;
using Java.Lang;

namespace InstagramDemo.Android
{
	public class InstagramImageAdapter : BaseAdapter 
	{
		private Context context;
		private InstagramMedia[] media;

		public InstagramImageAdapter(Context c, InstagramMedia[] media) 
		{
			context = c;
			this.media = media;
		}

		public override int Count 
		{
			get 
			{
				return media.Length;
			}
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return null;
		}

		public override long GetItemId(int position) 
		{
			return 0;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ImageView imageView = null;

			if (convertView == null)
			{
				imageView = new ImageView(context);
				imageView.LayoutParameters = new GridView.LayoutParams(200, 200);
				imageView.SetScaleType(ImageView.ScaleType.FitCenter);
				imageView.SetPadding(8, 8, 8, 8);
			} 
			else 
			{
				imageView = (ImageView)convertView;
			}

			DownloadImageTask task = new DownloadImageTask(imageView);
			task.Execute(this.media[position].Images.ThumbnailImage.URL);

			return imageView;
		}

		private class DownloadImageTask : AsyncTask<string, Void, Bitmap> 
		{
			private ImageView imageView;

			public DownloadImageTask(ImageView imageView)
			{
				this.imageView = imageView;
			}

			#region implemented abstract members of AsyncTask

			protected override Bitmap RunInBackground(params string[] @params)
			{
				URL thumbnailURL = new URL(@params[0]);
				return BitmapFactory.DecodeStream(thumbnailURL.OpenConnection().InputStream);
			}

			protected override void OnPostExecute(Bitmap result)
			{
				this.imageView.SetImageBitmap(result);
			}

			#endregion
		}
	}
}

