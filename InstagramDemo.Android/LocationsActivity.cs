using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using InstagramDemo.Core;
using System.Threading;

namespace InstagramDemo.Android
{
	[Activity (Label = "Locations", MainLauncher = true)]
	public class LocationsActivity : Activity, AdapterView.IOnItemClickListener
	{
		private List<InstagramLocation> locations;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.Locations);

			Instagram.GetLocations(55, 37, locations => {
				this.locations = new List<InstagramLocation>();
				var locationsAdapter = new ArrayAdapter<String>(this, Resource.Layout.LocationsListViewItem, Resource.Id.textView1);
				foreach (var location in locations) {
					this.locations.Add(location);
					locationsAdapter.Add(location.Name);
				}

				ListView listView = this.FindViewById<ListView>(Resource.Id.listView1);
				listView.Adapter = locationsAdapter;
				listView.OnItemClickListener = this;
			});
		}

		public void OnItemClick(AdapterView parent, View view, int position, long id)
		{
			Intent intent = new Intent(this, typeof(PhotosActivity));
			InstagramLocation location = this.locations[position];
			Bundle bundle = new Bundle();
			bundle.PutInt("id", location.Id);
			bundle.PutString("name", location.Name);
			intent.PutExtras(bundle);
			this.StartActivity(intent);
		}
	}
}


