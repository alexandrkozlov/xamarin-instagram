using System;
using System.Runtime.Serialization;

namespace InstagramDemo.Core
{
	[DataContract]
	public class InstagramMediaData
	{
		[DataMember(Name = "data")]
		public InstagramMedia[] Media { get; set; }
	}

	[DataContract]
	public class InstagramMedia
	{
		[DataMember(Name = "caption")]
		public InstagramMediaCaption Caption { get; set; }

		[DataMember(Name = "images")]
		public InstagramMediaImages Images { get; set; }
	}

	[DataContract]
	public class InstagramMediaCaption
	{
		[DataMember(Name = "text")]
		public string Text { get; set; }
	}

	[DataContract]
	public class InstagramMediaImages
	{
		[DataMember(Name = "thumbnail")]
		public InstagramMediaImage ThumbnailImage { get; set; }

		[DataMember(Name = "low_resolution")]
		public InstagramMediaImage LowResolutionImage { get; set; }

		[DataMember(Name = "standard_resolution")]
		public InstagramMediaImage StandardResolutionImage { get; set; }
	}

	[DataContract]
	public class InstagramMediaImage
	{
		[DataMember(Name = "url")]
		public string URL { get; set; }

		[DataMember(Name = "width")]
		public int Width { get; set; }

		[DataMember(Name = "height")]
		public int Height { get; set; }
	}
}


//"images":  {
//				"low_resolution":  {
//					"url": "http://distilleryimage1.s3.amazonaws.com/68c31676397411e394a422000a1f9874_6.jpg",
//					"width": 306,
//					"height": 306
//				},
//				"thumbnail":  {
//					"url": "http://distilleryimage1.s3.amazonaws.com/68c31676397411e394a422000a1f9874_5.jpg",
//					"width": 150,
//					"height": 150
//				},
//				"standard_resolution":  {
//					"url": "http://distilleryimage1.s3.amazonaws.com/68c31676397411e394a422000a1f9874_8.jpg",
//					"width": 640,
//					"height": 640
//				}
//			},

//{
//	"pagination":  {},
//	"meta":  {
//		"code": 200
//	},
//	"data":  [
//		{
//			"attribution": null,
//			"tags":  [],
//			"location":  {},
//			"comments":  {},
//			"filter": "Sutro",
//			"created_time": "1383064966",
//			"link": "http://instagram.com/p/gDvT7HFf6J/",
//			"likes":  {},
//			"images":  {},
//			"users_in_photo":  [],
//			"caption":  {
//				"created_time": "1383065184",
//				"text": "Жонглёр в пробке",
//				"from":  {
//					"username": "voko_",
//					"profile_picture": "http://images.ak.instagram.com/profiles/profile_185211406_75sq_1378630457.jpg",
//					"id": "185211406",
//					"full_name": "Kosinenko Vladimir"
//				},
//				"id": "577515082730569302"
//			},
//			"type": "image",
//			"id": "577513254559284873_185211406",
//			"user":  {
//				"username": "voko_",
//				"website": "",
//				"profile_picture": "http://images.ak.instagram.com/profiles/profile_185211406_75sq_1378630457.jpg",
//				"full_name": "Kosinenko Vladimir",
//				"bio": "",
//				"id": "185211406"
//			}
//		},
//		{
//			"attribution": null,
//			"tags":  [
//				"чавес",
//				"уго",
//				"москва"
//			],
//			"location":  {
//				"latitude": 55,
//				"name": "Mosfilm",
//				"longitude": 37,
//				"id": 90839910
//			},
//			"comments":  {
//				"count": 3,
//				"data":  [
//					{
//						"created_time": "1382290059",
//						"text": "Дад",
//						"from":  {
//							"username": "mnogolika_nika",
//							"profile_picture": "http://images.ak.instagram.com/profiles/profile_388024703_75sq_1369307115.jpg",
//							"id": "388024703",
//							"full_name": "mnogolika.nika"
//						},
//						"id": "571012864162779747"
//					},
//					{
//						"created_time": "1382290072",
//						"text": "Даже такая есть?!",
//						"from":  {
//							"username": "mnogolika_nika",
//							"profile_picture": "http://images.ak.instagram.com/profiles/profile_388024703_75sq_1369307115.jpg",
//							"id": "388024703",
//							"full_name": "mnogolika.nika"
//						},
//						"id": "571012972073833068"
//					},
//					{
//						"created_time": "1382290132",
//						"text": "@mnogolika_nika внезапно! Сам удивлен!",
//						"from":  {
//							"username": "nyptus",
//							"profile_picture": "http://images.ak.instagram.com/profiles/profile_33131651_75sq_1376425483.jpg",
//							"id": "33131651",
//							"full_name": "Maxim"
//						},
//						"id": "571013474736001675"
//					}
//				]
//			},
//			"filter": "Amaro",
//			"created_time": "1382265788",
//			"link": "http://instagram.com/p/fr7ADQGdE1/",
//			"likes":  {
//				"count": 12,
//				"data":  [
//					{
//						"username": "2kn27",
//						"profile_picture": "http://images.ak.instagram.com/profiles/profile_339144641_75sq_1370207800.jpg",
//						"id": "339144641",
//						"full_name": "2kn27"
//					},
//					{
//						"username": "sergeyazeyev",
//						"profile_picture": "http://images.ak.instagram.com/profiles/profile_220321107_75sq_1371151543.jpg",
//						"id": "220321107",
//						"full_name": "GreatGatsby"
//					},
//					{
//						"username": "ap4575",
//						"profile_picture": "http://images.ak.instagram.com/profiles/profile_355715446_75sq_1366407993.jpg",
//						"id": "355715446",
//						"full_name": "Pete:FreedomPeopleConstitution"
//					},
//					{
//						"username": "superbbarok",
//						"profile_picture": "http://images.ak.instagram.com/profiles/profile_418127303_75sq_1371220365.jpg",
//						"id": "418127303",
//						"full_name": "superbarok"
//					}
//				]
//			},
//			"images":  {
//				"low_resolution":  {
//					"url": "http://distilleryimage1.s3.amazonaws.com/68c31676397411e394a422000a1f9874_6.jpg",
//					"width": 306,
//					"height": 306
//				},
//				"thumbnail":  {
//					"url": "http://distilleryimage1.s3.amazonaws.com/68c31676397411e394a422000a1f9874_5.jpg",
//					"width": 150,
//					"height": 150
//				},
//				"standard_resolution":  {
//					"url": "http://distilleryimage1.s3.amazonaws.com/68c31676397411e394a422000a1f9874_8.jpg",
//					"width": 640,
//					"height": 640
//				}
//			},
//			"users_in_photo":  [],
//			"caption":  {
//				"created_time": "1382265833",
//				"text": "О как! #Уго #Чавес #Москва",
//				"from":  {
//					"username": "nyptus",
//					"profile_picture": "http://images.ak.instagram.com/profiles/profile_33131651_75sq_1376425483.jpg",
//					"id": "33131651",
//					"full_name": "Maxim"
//				},
//				"id": "570809641921531984"
//			},
//			"type": "image",
//			"id": "570809266028007733_33131651",
//			"user":  {
//				"username": "nyptus",
//				"website": "",
//				"profile_picture": "http://images.ak.instagram.com/profiles/profile_33131651_75sq_1376425483.jpg",
//				"full_name": "Maxim",
//				"bio": "",
//				"id": "33131651"
//			}
//		}
//	]
//}
