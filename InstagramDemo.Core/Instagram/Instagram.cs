using System;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Threading;

namespace InstagramDemo.Core
{
	public class Instagram
	{
		private const string Endpoint = @"https://api.instagram.com/v1";
		private const string ClientID = @"4b860334aec54f888ecea63e3a72fddd";


		public delegate void InstagranLocationsHandler(InstagramLocation[] locations);

		public static void GetLocations(double latitude, double longitude, InstagranLocationsHandler handler)
		{
			SynchronizationContext synchronizationContext = SynchronizationContext.Current;

			RestRequest restRequest = new RestRequest();
			restRequest.Endpoint = Endpoint;
			restRequest.Request = "locations/search";
			restRequest.AddParameter ("lat", latitude.ToString());
			restRequest.AddParameter ("lng", longitude.ToString());
			restRequest.AddParameter ("client_id", ClientID);

			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(restRequest.Uri);
			webRequest.BeginGetResponse(result => {
				HttpWebResponse responce = (result.AsyncState as HttpWebRequest).EndGetResponse(result) as HttpWebResponse;
				DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(InstagramResponce));
				object objResponse = jsonSerializer.ReadObject(responce.GetResponseStream());
				InstagramResponce locationsResponse = objResponse as InstagramResponce;

				if (synchronizationContext != null) {
					synchronizationContext.Post(state => {
						if (handler != null) {
							handler(locationsResponse.Locations);
						}
					}, null);
				}
				else {
					if (handler != null) {
						handler(locationsResponse.Locations);
					}
				}

			}, webRequest).AsyncWaitHandle.WaitOne();
		}


		public delegate void InstagramMediaDataHandler(InstagramMedia[] media);

		public static void GetMediaForLocation(int locationId, InstagramMediaDataHandler handler)
		{
			SynchronizationContext synchronizationContext = SynchronizationContext.Current;

			RestRequest restRequest = new RestRequest ();
			restRequest.Endpoint = Endpoint;
			restRequest.Request = "locations/" + locationId + "/media/recent";
			restRequest.AddParameter ("client_id", ClientID);

			TimeSpan span = (DateTime.Now.AddYears(-5) - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
			restRequest.AddParameter("min_timestamp", span.TotalSeconds.ToString());

			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(restRequest.Uri);
			webRequest.BeginGetResponse(new AsyncCallback(result => {
				HttpWebResponse responce = (result.AsyncState as HttpWebRequest).EndGetResponse(result) as HttpWebResponse;
				DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(InstagramMediaData));
				InstagramMediaData mediaResponce = (InstagramMediaData)jsonSerializer.ReadObject(responce.GetResponseStream());

				if (synchronizationContext != null) {
					synchronizationContext.Post(state => {
						if (handler != null) {
							handler(mediaResponce.Media);
						}
					}, null);
				}
				else {
					if (handler != null) {
						handler(mediaResponce.Media);
					}
				}

			}), webRequest).AsyncWaitHandle.WaitOne();
		}
	}
}

