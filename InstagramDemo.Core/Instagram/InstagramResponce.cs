using System;
using System.Runtime.Serialization;

namespace InstagramDemo.Core
{
	[DataContract]
	public class InstagramResponce
	{
		[DataMember(Name = "meta")]
		public InstagramResponceMetadata Meta { get; set; }

		[DataMember(Name = "data")]
		public InstagramLocation[] Locations { get; set; }
	}

	[DataContract]
	public class InstagramResponceMetadata
	{
		[DataMember(Name = "code")]
		public int Code { get; set; }
	}

	[DataContract]
	public class InstagramLocation
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		[DataMember(Name = "latitude")]
		public double Latitude { get; set; }

		[DataMember(Name = "longitude")]
		public double Longitude { get; set; }

		[DataMember(Name = "name")]
		public string Name { get; set; }
	}
}
