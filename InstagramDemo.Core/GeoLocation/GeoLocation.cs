using System;

namespace InstagramDemo.Core
{
	public struct GeoLocation
	{
		public double Latitude;
		public double Longitude;

		public GeoLocation(double latitude, double longitude)
		{
			this.Latitude = latitude;
			this.Longitude = longitude;
		}
	}
}

