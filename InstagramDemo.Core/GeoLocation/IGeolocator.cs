using System;

namespace InstagramDemo.Core
{
	public interface IGeoLocator
	{
		GeoLocation CurrentLocation { get; }
	}
}

