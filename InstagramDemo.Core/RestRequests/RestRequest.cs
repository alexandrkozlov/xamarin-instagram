using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace InstagramDemo.Core
{
	public class RestRequest
	{
		private Dictionary<String, String> parameters = new Dictionary<String, String>();

		public string Endpoint { get; set; }
		public string Request { get; set; }

		public void AddParameter(string name, string value)
		{
			this.parameters.Add (name, value);
		}

		public string Uri
		{
			get
			{
				StringBuilder uriBuiler = new StringBuilder ();
				uriBuiler.Append (this.Endpoint);
				uriBuiler.Append ("/");
				uriBuiler.Append (this.Request);

				foreach (string key in this.parameters.Keys)
				{
					if (key == this.parameters.Keys.First ())
					{
						uriBuiler.Append ("?");
					}
					else
					{
						uriBuiler.Append ("&");
					}

					uriBuiler.AppendFormat ("{0}={1}", key, this.parameters [key]);
				}

				return uriBuiler.ToString ();
			}
		}
	}
}

