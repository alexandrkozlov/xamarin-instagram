using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace InstagramDemo
{
	public class LocationsTableViewControllerCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("LocationsTableViewControllerCell");

		public LocationsTableViewControllerCell () : base (UITableViewCellStyle.Value1, Key)
		{
			// TODO: add subviews to the ContentView, set various colors, etc.
			TextLabel.Text = "TextLabel";
		}
	}
}

