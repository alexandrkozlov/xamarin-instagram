using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using InstagramDemo.Core;
using System.Collections.Generic;

namespace InstagramDemo
{
	public class LocationsTableViewControllerController : UITableViewController
	{
		public LocationsTableViewControllerController () : base (UITableViewStyle.Grouped)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			Instagram instagram = new Instagram ();
			instagram.GetLocations (new GeoLocation (55, 37), LocationsLoaded);
		}

		private void LocationsLoaded(IEnumerable<InstagramLocation> locations)
		{
			this.TableView.ReloadData ();
		}

		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			// TODO: return the actual number of items in the section
			return 1;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return "Header";
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			return "Footer";
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (LocationsTableViewControllerCell.Key) as LocationsTableViewControllerCell;
			if (cell == null)
				cell = new LocationsTableViewControllerCell ();

			// TODO: populate the cell with the appropriate data based on the indexPath
			cell.DetailTextLabel.Text = "DetailsTextLabel";

			return cell;
		}
	}
}

