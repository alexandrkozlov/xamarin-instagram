using NUnit.Framework;
using System;

namespace InstagramDemo.Core.Tests
{
	[TestFixture()]
	public class InstagramTests
	{
		[Test()]
		public void GetLocations()
		{
			Instagram.GetLocations(55, 37, locations => {
				foreach (var location in locations) {
					Console.WriteLine(location.Name);
				}
			});
		}

		[Test()]
		public void GetMediaForLocation()
		{
			Instagram.GetMediaForLocation(46591653, medias => {
				foreach (var media in medias) {
					Console.WriteLine(media.Caption.Text);
				}
			});
		}
	}
}

